from crawler import inventory
from crawler.crawler import Crawler
from multiprocessing import Pool
import time


def main():
    FILTER = ['tops-sweaters']
    ROOT_URL = 'https://supremenewyork.com'
    SHOP_URL = 'https://supremenewyork.com/shop'
    product_urls = inventory.get_all_product_urls(SHOP_URL, FILTER)

    start = time.time()
    items = []
    print('Adding all items to item list...')
    with Pool(10) as p:
        items = p.map(inventory.get_product, product_urls)

    print('Added all items!')
    print('Item list length is {}'.format(len(items)))
    print('----')
    for item in items:
        print('[+] {}'.format(item.get_name()))
        print('----> {} VARIATIONS'.format(len(item.get_variations())))
    end = time.time()
    print('FINISHED IN {}'.format(end - start))

    crawler = Crawler(items)
    if(crawler.start()):
        crawler.add_desired_to_cart()
    crawler.checkout_now()


if __name__ == '__main__':
    main()
