class Variation():

    def __init__(self, name, url, attributes=None):
        self.name = name
        self.url = url
        self.attributes = attributes

    def get_name(self):
        return self.name

    def get_url(self):
        return self.url

    def get_attributes(self):
        return self.attributes

    def set_name(self, name):
        self.name = name

    def set_url(self, url):
        self.url = url

    def set_attributes(self, attributes):
        self.attributes = attributes
