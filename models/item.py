class Item():
    def __init__(self, name, url, price, variations=None):
        self.name = name
        self.url = url
        self.price = price
        self.variations = variations

    def get_name(self):
        return self.name

    def get_url(self):
        return self.url

    def get_price(self):
        return self.price

    def get_variations(self):
        return self.variations

    def set_name(self, name):
        self.name = name

    def set_url(self, url):
        self.url = url

    def set_price(self, price):
        self.price = price

    def set_variatons(self, variations):
        self.variations = variations
