echo -e "\x1B[31m---- INSTALLING SHOE BOT DEPENDENCIES ----\x1B[0m"
echo -e "\x1B[31m[+] Installing home brew...\x1B[0m"
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
echo -e "\x1B[31m[+] Installing Python...\x1B[0m"
brew install python
echo -e "\x1B[31m[+] Installing pip modules...\x1B[0m"
source venv/bin/activate
pip install -r requirements.txt
echo -e "\x1B[31mINSTALL COMPLETE!\x1B[0m"