# Installation:

1. Open a terminal (press space + cmd and type "terminal" then press enter)
2. type ```git clone https://yougotdslapd@bitbucket.org/yougotdslapd/shoebot.git```
3. type ```cd shoebot```
4. type ```./install.sh``` and let the installer do what it has to do. answer the prompts where necessary
5. after the installer finishes, type ```python product_tests.py```
