from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from time import sleep


class Crawler():

    def __init__(self, items):
        self.items = items

    def start(self):
        try:
            self.driver = webdriver.Chrome()
        except:
            return
        return self.driver

    def add_desired_to_cart(self):
        for item in self.items:
            if(len(item.get_variations()) > 0):
                for variation in item.get_variations():
                    if(variation.get_name().lower() == 'black'):
                        print(variation.get_url())
                        try:
                            self.driver.get(
                                'https://supremenewyork.com{}'.format(variation.get_url()))
                            add_to_cart = self.driver.find_element_by_xpath(
                                """//*[@id="add-remove-buttons"]/input""")
                        except:
                            print('FAILED!')
                            exit()
                        self.driver.implicitly_wait(3)
                        add_to_cart.click()
                        sleep(1.5)

    def checkout_now(self):
        try:
            self.driver.implicitly_wait(1)
            checkout = self.driver.find_element_by_link_text('checkout now')
        except:
            exit()
        checkout.click()
        sleep(1.5)
