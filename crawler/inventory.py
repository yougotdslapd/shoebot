from bs4 import BeautifulSoup
from multiprocessing import Pool
from models.item import Item
from models.variation import Variation
import requests

BASE_URL = 'https://supremenewyork.com'


def get_all_product_urls(url, categories):
    product_urls = []
    try:
        request = requests.get(url, timeout=10)
    except:
        return
    soup = BeautifulSoup(request.text, 'html.parser')
    for link in soup.find_all('a'):
        if(('http' or 'https') not in link.get('href')
                and link.get('href') != '/'
                and 'cart' not in link.get('href')):
            for category in categories:
                if category in link.get('href'):
                    product_urls.append('{}{}'.format(
                        BASE_URL, link.get('href')))
    return product_urls


def get_product(url):
    try:
        request = requests.get(url, timeout=10)
    except:
        return
    soup = BeautifulSoup(request.text, 'html.parser')
    title = soup.title.string
    title = title.replace('Supreme: ', '')
    title = title.split(' - ')[0]
    """for keyword in filter:
        if(keyword not in title.lower()):
            return"""
    price = soup.find('p', {'class': 'price'}).find('span').string
    item = Item(title, url, price)

    variations = []
    try:
        ul = soup.find('ul', {'class': 'styles'})
        for li in ul.find_all('li'):
            sold_out_bool = li.find('a').get('data-sold-out')
            if(sold_out_bool == 'false'):
                variation = Variation(li.find('a').get(
                    'data-style-name'), li.find('a').get('href'))
                variations.append(variation)
    except:
        pass

    item.set_variatons(variations)
    return item
